import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class FileCopierWithCamel{

	public static void main(String args[]) throws Exception{
		
		//JAVA DSL
		/*CamelContext context = new DefaultCamelContext();
		context.addRoutes(new RouteBuilder() {
			public void configure() {
				from("file:data/inbox?noop=true")
				.to("file:data/outbox");
			}
		});*/
		//END JAVA DSL
		
		//SPRING DSL
		//context defined into src/main/resources/applicationContext-camel.xml
		AbstractApplicationContext context = new ClassPathXmlApplicationContext("applicationContext-camel.xml");
		context.start();
        System.out.println("Application context started");
		//END SPRING DSL
		context.start();
		Thread.sleep(10000);
		context.stop();
	}
}

